﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using UnityEngine;
using Reptile;
using Reptile.Phone;
using BRCML;
using BRCML.Audio;

namespace TrackRemix
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(BRCML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInProcess("Bomb Rush Cyberfunk.exe")]
    public class TrackRemix : BaseUnityPlugin
    {
        internal static TrackRemix Instance { get; private set; } = null;
        internal static DirectoryInfo ModdingFolder { get; private set; } = null;
        internal static ManualLogSource Log { get; private set; } = null;
        internal static AudioCache musicCache = new AudioCache();
        private static AudioManager audioManager = null;

        internal static ConfigEntry<bool> enableVanillaMusics;
        internal static ConfigEntry<KeyCode> reloadLibraryKey;


        private static DirectoryInfo SongsDirectory => Directory.CreateDirectory(Path.Combine(ModdingFolder.FullName, "Songs"));
        private static DirectoryInfo GlobalSongsDirectory => Directory.CreateDirectory(Path.Combine(SongsDirectory.FullName, "Global"));


        // Awake is called once when both the game libs and the plug-in are loaded
        void Awake()
        {
            Instance = this;
            Log = this.Logger;
            Logger.LogInfo("Hello, world!");

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            Logger.LogDebug("Patching LoadInLibrary");
            harmoInstance.PatchAll(typeof(LoadInLibrary));


            enableVanillaMusics = this.Config.Bind<bool>("Toggles", "EnableVanillaMusics", true);
            reloadLibraryKey = this.Config.Bind<KeyCode>("KeyBinds", "ReloadLibrary", KeyCode.F9);
        }

        void Start()
        {
            ModdingFolder = BRCML.Utils.ModdingFolder.GetModSubFolder(this.Info);

            LoadMusics();
        }

        void Update()
        {
            if (audioManager == null)
            {
                audioManager = Core.instance?.audioManager;
            }
            if (Input.GetKeyDown(reloadLibraryKey.Value))
            {
                LoadMusics();
            }
            var trackList = (Core.instance?.AudioManager?.MusicPlayer as MusicPlayer)?.musicTrackQueue?.currentMusicTracks;
            if (trackList != null)
            {
                foreach (MusicTrack track in trackList)
                {
                    if (track.AudioClip.samples == 0)
                    {
                        foreach(var key in musicCache.Keys())
                        {

                            var matchingAsset = musicCache.GetAssets(key).Find((asset) => asset.GetHashCode().ToString() == track.Uid);
                            if (matchingAsset != null)
                            {
                                track.AudioClip = AudioUtils.CloneAudioClip(matchingAsset.audioClip, matchingAsset.audioClip.name + (trackid++));

                            }
                        }
                    }
                }
            }
        }

        private void LoadMusics()
        {
            musicCache.Clear();

            Logger.LogInfo($"Loading Global folder at: {GlobalSongsDirectory}");
            foreach (AudioInfo musicInfo in AudioUtils.GetAudioInfos(GlobalSongsDirectory))
            {
                Logger.LogDebug("Loading new " + GlobalSongsDirectory.Name + " : " + musicInfo.file.FullName);
                musicCache.LoadClip(GlobalSongsDirectory.Name, musicInfo);
            }

            for (int i = (int) Stage.NONE + 1; i < (int)Stage.MAX; i++)
            {

                DirectoryInfo tapeDir = SongsDirectory.CreateSubdirectory(((Stage)i).ToString());
                foreach (AudioInfo musicInfo in AudioUtils.GetAudioInfos(tapeDir))
                {
                    Logger.LogDebug("Loading new " + tapeDir.Name + " : " + musicInfo.file.FullName);
                    musicCache.LoadClip(tapeDir.Name, musicInfo);
                }
            }

        }

        public static string TrackToString(MusicTrack track)
        {
            return $"{track.Title} by {track.Artist} | UID: {track.Uid}, AC:{track.AudioClip}";
        }

        private int trackid = 0;
        public void AddTracks(IMusicPlayer musicPlayer)
        {
            MusicPlayer mp = musicPlayer as MusicPlayer;
            var trackList = mp.musicTrackQueue.currentMusicTracks;
            Log.LogDebug("Building track list");
            if (musicCache.ContainsKey("Global"))
            {
                foreach (AudioAsset asset in musicCache.GetAssets("Global"))
                {
                    MusicTrack track = trackList.Find((t) => t.Uid == asset.GetHashCode().ToString());
                    if (track != null)
                    {
                        Log.LogDebug($"Track will be edited: {TrackToString(track)}");
                        track.AudioClip = asset.audioClip;
                    }
                    else
                    {
                        AddMusicTrack(MakeNewTrackFrom(asset), mp);
                    }
                }
            }
            if (BaseModule.instance?.currentStage != null)
            {
                if (musicCache.ContainsKey(BaseModule.instance?.currentStage.ToString()))
                {
                    foreach (AudioAsset asset in musicCache.GetAssets(BaseModule.instance?.currentStage.ToString()))
                    {
                        MusicTrack track = trackList.Find((t) => t.Uid == asset.GetHashCode().ToString());
                        if (track != null)
                        {
                            Log.LogDebug($"Track will be edited: {TrackToString(track)}");
                            track.AudioClip = asset.audioClip;
                        }
                        else
                        {
                            AddMusicTrack(MakeNewTrackFrom(asset), mp);
                        }
                    }
                }
            }
        }

        private void AddMusicTrack(MusicTrack track, MusicPlayer player)
        {
            player.AddMusicTrack(track);
        }

        private MusicTrack MakeNewTrackFrom(AudioAsset asset)
        {
            MusicTrack newTrack = ScriptableObject.CreateInstance<MusicTrack>();
            newTrack.name = "MusicTrack_" + asset.audioClip.name;
            newTrack.AudioClip = asset.audioClip;
            newTrack.isRepeatable = false;
            newTrack.Title = asset.audioInfo.title;
            newTrack.Artist = asset.audioInfo.author;
            newTrack.Uid = asset.GetHashCode().ToString();
            newTrack.IsDefault = false;
            return newTrack;
        }

        private List<AudioAsset> GetAllMusic()
        {
            var list = new List<AudioAsset>();
            foreach (string key in musicCache.Keys())
            {
                foreach (AudioAsset asset in musicCache.GetAssets(key))
                {
                    list.Add(asset);
                }
            }
            return list;
        }
        public static AudioClip CloneAudioClip(AudioClip audioClip, string newName)
        {
            AudioClip newAudioClip = AudioClip.Create(newName, audioClip.samples, audioClip.channels, audioClip.frequency, false);
            float[] copyData = new float[audioClip.samples * audioClip.channels];
            audioClip.GetData(copyData, 0);
            newAudioClip.SetData(copyData, 0);
            return newAudioClip;
        }
    }
}
