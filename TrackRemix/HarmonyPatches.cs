﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Reptile;
using Reptile.Phone;
using HarmonyLib;
using BRCML.Audio;

namespace TrackRemix
{

    public static class MainMenuMusic
    {

        [HarmonyPatch(typeof(BaseModule), nameof(BaseModule.StartMainMenuSceneMusic))]
        [HarmonyPrefix]
        public static bool RefreshUnlockedMusicTracks_postfix(AppMusicPlayer __instance)
        {
            return true;
        }
    }

    public static class StageStartMusic
    {

        [HarmonyPatch(typeof(StageManager), nameof(StageManager.StartOrContinueStageTrack))]
        [HarmonyPrefix]
        public static bool RefreshUnlockedMusicTracks_postfix(StageManager __instance)
        {
            return true;
        }
    }

    public static class LoadInLibrary
    {
        [HarmonyPatch(typeof(MusicLibrary), nameof(MusicLibrary.GenerateEnumDictionaries))]
        [HarmonyPrefix]
        public static bool GenerateEnumDictionaries_Prefix(MusicLibrary __instance)
        {
            foreach( var pair in __instance.musicTrackDictionary)
            {
                TrackRemix.Log.LogDebug($"TrackId:{pair.Key} | Track: {pair.Value.Title}");
            }
            return true;
        }
        /*
        [HarmonyPatch(typeof(MusicPlayer), nameof(MusicPlayer.PlayPlaylist))]
        [HarmonyPatch(typeof(MusicPlayer), nameof(MusicPlayer.PlayFromStart))]
        [HarmonyPatch(typeof(MusicPlayer), nameof(MusicPlayer.AddMusicTrack))]
        */       
        [HarmonyPatch(typeof(MusicPlayer), nameof(MusicPlayer.StartMusicPlayer))]
        [HarmonyPrefix]
        public static bool Play_Prefix(MusicPlayer __instance)
        {
            //BRCML.Utils.DebugUtils.PrintStacktrace();
            TrackRemix.Log.LogDebug($"StartMusicPlayer");
            foreach (var track in __instance.musicTrackQueue.currentMusicTracks)
            {
                TrackRemix.Log.LogDebug($"Track: {TrackRemix.TrackToString(track)}");
            }
            return true;
        }

        [HarmonyPatch(typeof(AppMusicPlayer), nameof(AppMusicPlayer.RefreshUnlockedMusicTracks))]
        [HarmonyPrefix]
        public static bool RefreshUnlockedMusicTracks_Prefix(AppMusicPlayer __instance)
        {
            if (!TrackRemix.enableVanillaMusics.Value)
            {

                //BRCML.Utils.DebugUtils.PrintStacktrace();
                TrackRemix.Log.LogDebug($"RefreshUnlockedMusicTracksPre");
                TrackRemix.Instance.AddTracks(__instance.GameMusicPlayer);
                foreach (var track in (__instance.GameMusicPlayer as MusicPlayer).musicTrackQueue.currentMusicTracks)
                {
                    TrackRemix.Log.LogDebug($"Track: {TrackRemix.TrackToString(track)}");
                }
                return false;
            }
            return true;
        }

        [HarmonyPatch(typeof(AppMusicPlayer), nameof(AppMusicPlayer.RefreshUnlockedMusicTracks))]
        [HarmonyPostfix]
        public static void RefreshUnlockedMusicTracks_Postfix(AppMusicPlayer __instance)
        {
            if (TrackRemix.enableVanillaMusics.Value)
            {
                //BRCML.Utils.DebugUtils.PrintStacktrace();
                TrackRemix.Instance.AddTracks(__instance.GameMusicPlayer);
                TrackRemix.Log.LogDebug($"RefreshUnlockedMusicTracksPost");
                foreach (var track in (__instance.GameMusicPlayer as MusicPlayer).musicTrackQueue.currentMusicTracks)
                {
                    TrackRemix.Log.LogDebug($"Track: {TrackRemix.TrackToString(track)}");
                }
            }
        }


        [HarmonyPatch(typeof(MusicPlayerBuffer), nameof(MusicPlayerBuffer.UnloadMusicPlayerData))]
        [HarmonyPrefix]
        static bool UnloadMusicPlayerData_Prefix(MusicPlayerData musicPlayerData) // the the game to not unload our files please lol
        {
            if (CacheContains(musicPlayerData) is AudioAsset track)
            {
                Core.instance?.audioManager.MusicPlayer.ForcePaused();
                return false;
            }
            return true;
        }

        public static AudioAsset CacheContains(MusicPlayerData mpData)
        {
            if (TrackRemix.musicCache.ContainsKey("Global"))
            {
                return TrackRemix.musicCache.GetAssets("Global").Find(m => mpData.Artist == m.audioInfo.author && mpData.Title == m.audioInfo.title);
            }
            if (BaseModule.instance != null && TrackRemix.musicCache.ContainsKey(BaseModule.instance.currentStage.ToString()))
            {
                return TrackRemix.musicCache.GetAssets(BaseModule.instance.currentStage.ToString()).Find(m => mpData.Artist == m.audioInfo.author && mpData.Title == m.audioInfo.title);
            }
            return null;
        }
    }
}
