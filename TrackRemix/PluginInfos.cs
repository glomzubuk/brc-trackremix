﻿using System.Reflection;
using TrackRemix;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace TrackRemix
{
    internal static class PluginInfos
    {
        public const string PLUGIN_NAME = "TrackRemix";
        public const string PLUGIN_ID = "fr.glomzubuk.plugins.brc.trackremix";
        public const string PLUGIN_VERSION = "0.1.2";
    }
}
