TrackRemix for Bomb Rush Cyberfunl, the one stop shop for all your audio needs.

Supported Formats are MP3, OGG, WAV and Aiff.

Default path to the folder where drop the songs is `<Game's Root>/ModdingFolder/TrackRemix/Songs`
It will be generated automatically by launching the game once after installing the mods. 

Then you can drop them in the zone you want to see them in, or in the Global folder to have them everywhere.

File name will be used for displaying the song title and artist in the following manner:
`<SongTitle>_<SongArtist>.<extension>`
